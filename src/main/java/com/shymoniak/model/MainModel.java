package com.shymoniak.model;

import com.shymoniak.model.entity.BinaryTree;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainModel {
    private static final Logger LOGGER = LogManager.getLogger(MainModel.class);
    private BinaryTree binaryTree = new BinaryTree();
    private TreePrinter treePrinter = new TreePrinter();

    public void addElement(int key, String value) {
        if (binaryTree.find(key) == null){
            binaryTree.insert(key, value);
        } else {
            LOGGER.error("Element with such key already exist!");
        }
    }

    public void deleteElement(int key) {
        if (binaryTree.find(key) != null) {
//            LOGGER.info("Deleted? - " + binaryTree.delete(key));
//            binaryTree = binaryTree.delete(key, binaryTree);
            binaryTree.deleteKey(key);
        } else {
            LOGGER.error("There is no element with such key!");
        }
    }

    public void printBinaryTree() {
        treePrinter.print(binaryTree.getRoot(), 0);
    }

    public void findElement(int key) {
        LOGGER.trace(binaryTree.find(key));
    }
}
