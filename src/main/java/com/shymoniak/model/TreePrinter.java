package com.shymoniak.model;

import com.shymoniak.model.entity.Node;

public class TreePrinter {

    public void print(Node root, int level) {
        if (root == null)
            return;
        print(root.getRightChild(), level + 1);
        if (level != 0) {
            for (int i = 0; i < level - 1; i++)
                System.out.print("|\t\t");
            System.out.println("|-------" + root.getKey());
        } else
            System.out.println(root.getKey());
        print(root.getLeftChild(), level + 1);
    }

}
