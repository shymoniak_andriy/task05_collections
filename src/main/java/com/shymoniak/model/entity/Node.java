package com.shymoniak.model.entity;

import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Node {
    private int key;
    private String value;
    private Node leftChild;
    private Node rightChild;
}
