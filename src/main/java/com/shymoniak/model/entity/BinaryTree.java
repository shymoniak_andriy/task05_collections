package com.shymoniak.model.entity;

import lombok.Getter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class implements realization of binary tree
 */
@Getter
public class BinaryTree {
    private static final Logger LOGGER = LogManager.getLogger(BinaryTree.class.getName());
    private Node root;

    /**
     * Returns node found in a binary tree by it's key.
     * If node is not found - returns null.
     *
     * @param key
     * @return
     */
    public Node find(int key) {
        // Starting from tree root
        Node current = root;

        if (current == null) {
            return null;
        }
        //Comparing key given and nodes keys in binary tree until they are equal.
        while (current.getKey() != key) {
            if (current.getKey() > key) {
                current = current.getLeftChild();
            } else if (current.getKey() < key) {
                current = current.getRightChild();
            }
            if (current == null) {
                return null;
            }
        }
        return current;
    }

    public void insert(int key, String value) {
        Node newNode = new Node(key, value, null, null);

        // If tree is empty - the first added element becomes a root
        if (root == null) {
            root = newNode;
        } else {
            Node current = root;
            Node parent;

            // Searching for leaf
            while (true) {
                if (current.getKey() > key) {
                    parent = current;
                    current = current.getLeftChild();
                    if (current == null) {
                        parent.setLeftChild(newNode);
                        return;
                    }
                } else if (current.getKey() < key) {
                    parent = current;
                    current = current.getRightChild();
                    if (current == null) {
                        parent.setRightChild(newNode);
                        return;
                    }
                }
            }
        }
    }

    public void deleteKey(int key) {
        root = deleteRec(root, key);
    }

    /* A recursive function to insert a new key in BST */
    Node deleteRec(Node root, int key) {
        /* Base Case: If the tree is empty */
        if (root == null) return root;

        /* Otherwise, recur down the tree */
        if (key < root.getKey())
            root.setLeftChild(deleteRec(root.getLeftChild(), key));
        else if (key > root.getKey())
            root.setRightChild(deleteRec(root.getRightChild(), key));

            // if key is same as root's key, then This is the node
            // to be deleted
        else {
            // node with only one child or no child
            if (root.getLeftChild() == null)
                return root.getRightChild();
            else if (root.getRightChild() == null)
                return root.getLeftChild();

            // node with two children: Get the inorder successor (smallest
            // in the right subtree)
            root.setKey(minValue(root.getRightChild()));

            // Delete the inorder successor
            root.setRightChild(deleteRec(root.getRightChild(), root.getKey()));
        }

        return root;
    }

    int minValue(Node root) {
        int minv = root.getKey();
        while (root.getLeftChild() != null) {
            minv = root.getLeftChild().getKey();
            root = root.getLeftChild();
        }
        return minv;
    }
}
