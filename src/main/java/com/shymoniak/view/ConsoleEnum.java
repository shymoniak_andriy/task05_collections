package com.shymoniak.view;

public enum ConsoleEnum {
    SPARATION_LINE("------------------------------------") ,
    ALL_OPERATIONS(" Choose an operation: "),
    FIRST_OPERATION("\t\t1. Add element"),
    SECOND_OPERATION("\t\t2. Delete element"),
    THIRD_OPERATION("\t\t3. Find element"),
    FOURTH_OPERATION("\t\t4. Print tree"),
    NINETH_OPERATION("\t\t9. Exit")
    ;

    private final String text;

    ConsoleEnum(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
