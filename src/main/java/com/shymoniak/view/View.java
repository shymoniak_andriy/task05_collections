package com.shymoniak.view;

import com.shymoniak.model.MainModel;
import org.apache.logging.log4j.*;

import java.util.Scanner;

public class View {
    private static final Logger LOGGER = LogManager.getLogger(View.class);

    Scanner scan = new Scanner(System.in);
    private MainModel mainModel = new MainModel();

    public void startApp() {
//        LOGGER.trace("This is a trace message");
//        LOGGER.debug("This is a debug message");
//        LOGGER.info("This is an info message");
//        LOGGER.warn("This is a warn message");
//        LOGGER.error("This is an error message");
//        LOGGER.fatal("This is a fatal message");

        int fromUser = getUserChoice();

        while (fromUser != 9) {
            if (fromUser == 1) {
                LOGGER.trace("Enter key and value of new element");
                int key = scan.nextInt();
                String value = scan.next();
                mainModel.addElement(key, value);
            } else if (fromUser == 2) {
                LOGGER.trace("Enter key of element you want to delete");
                int key = scan.nextInt();
                mainModel.deleteElement(key);
            } else if (fromUser == 3) {
                LOGGER.trace("Enter key of element you want to find");
                int key = scan.nextInt();
                mainModel.findElement(key);
            } else if (fromUser == 4) {
                mainModel.printBinaryTree();
            } else if (fromUser == 9) {
                return;
            }
            fromUser = getUserChoice();
        }

    }

    private int getUserChoice() {
        LOGGER.info("\n" + ConsoleEnum.ALL_OPERATIONS +
                "\n" + ConsoleEnum.FIRST_OPERATION +
                "\n" + ConsoleEnum.SECOND_OPERATION +
                "\n" + ConsoleEnum.THIRD_OPERATION +
                "\n" + ConsoleEnum.FOURTH_OPERATION +
                "\n" + ConsoleEnum.NINETH_OPERATION);
        return scan.nextInt();
    }
}
